<?php

namespace Carica {

  use Carica\Io;
  use Carica\Chip;
  use React;
  use Ratchet;

  class SensorPhalanx implements Ratchet\MessageComponentInterface {

    use \Carica\Io\Event\Loop\Aggregation;

    private $_clients = array();
    private $_sensors = array();

    private $_server = NULL;

    public function __construct(array $sensors) {
      foreach ($sensors as $index => $sensor) {
        if ($sensor instanceOf Chip\Sensor\Analog) {
          $this->_sensors[$index] = $sensor;
        }
      }
    }

    public function listen($port = 8081) {
      $socket = new \React\Socket\Server($this->loop()->loop());
      $socket->listen($port);
      $this->_server = new Ratchet\Server\IoServer(
        new Ratchet\Http\HttpServer(
          new Ratchet\WebSocket\WsServer(
            $this
          )
        ),
        $socket,
        $this->loop()->loop()
      );
    }

    public function update($log = TRUE) {
      $values = ['type' => 'sensors'];
      foreach ($this->_sensors as $index => $sensor) {
        $values['sensors'][$index] = $sensor->get();
      }
      $json = json_encode($values);
      if ($log) {
        echo $json, "\n";
      }
      foreach ($this->_clients as $client) {
        $client->send($json);
      }
    }

    public function onOpen(Ratchet\ConnectionInterface $connection) {
      $this->_clients[spl_object_hash($connection)] = $connection;
    }

    public function onClose(Ratchet\ConnectionInterface $connection) {
      unset($this->_clients[spl_object_hash($connection)]);
    }

    public function onMessage(Ratchet\ConnectionInterface $connection, $message) {
    }

    public function onerror(Ratchet\ConnectionInterface $connection, \Exception $e) {
      echo "Error: ", $e->getMessage(), "\n";
      $connection->close();
    }
  }
}