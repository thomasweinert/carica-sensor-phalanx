<?php
use Carica\Chip as Chip;
use Carica\Io\Network\Http as Http;

$board = include(__DIR__.'/bootstrap.php');
$loop = Carica\Io\Event\Loop\Factory::get();

$board
  ->activate()
  ->done(
    function () use ($board, $loop) {
      // define some file delivery
      $route = new Http\Route();
      $route->match('/', new Http\Route\File(__DIR__.'/index.html'));
      $route->startsWith('/files', new Http\Route\Directory(__DIR__));
      $httpServer = new Http\Server($route);
      $httpServer->listen(8080);

      // create the phalanx with some sensors
      include(__DIR__.'/class/SensorPhalanx.php');
      $phalanx = new Carica\SensorPhalanx(
        [
          'lightsensor' => new Chip\Sensor\Analog($board->pins[15]),
          'potentiometer' => new Chip\Sensor\Analog($board->pins[16])
        ]
      );
      // tell the websocket server to listen
      $phalanx->listen(8081);

      // update clients connected to the phalanx with the current sensor data
      $loop->setInterval(
        function () use ($phalanx) {
          $phalanx->update();
        },
        200
      );
    }
  );

$loop->run();